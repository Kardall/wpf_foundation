﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WPF_Foundation.ViewModel;

namespace WPF_Foundation.Commands
{
    public class IncrementValueCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;
        MainWindowViewModel ViewModel;

        public IncrementValueCommand(MainWindowViewModel _vm)
        {
            ViewModel = _vm;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            ViewModel.AddANumber();
        }
    }
}
