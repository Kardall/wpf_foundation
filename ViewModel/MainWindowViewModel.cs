﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPF_Foundation.Commands;

namespace WPF_Foundation.ViewModel
{
    public class MainWindowViewModel : ViewModelBase
    {

        #region UI Components

        string _TextBlockText;
        public string TextBlockText
        {
            get => _TextBlockText;
            set
            {
                if (_TextBlockText == value) return;
                _TextBlockText = value;
                NotifyPropertyChanged(nameof(TextBlockText));
            }
        }

        #endregion

        #region Commands

        public IncrementValueCommand IncrementValue { get; set; }

        #endregion

        #region Command Functions

        int Number = 0;

        public void AddANumber()
        {
            Number++;
            TextBlockText = $"Clicks: {Number}";
        }

        #endregion

        public MainWindowViewModel()
        {
            IncrementValue = new IncrementValueCommand(this);

            TextBlockText = $"Clicks: {Number}";
        }
    }
}
